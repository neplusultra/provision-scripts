#!/usr/bin/env bash

#####################################
########## EXPO / RN SETUP ##########
#####################################
############### USAGE ###############
#####################################
#
# 1. Create a new workspace on Cloud9 using the "Blank" template
#
# 2. Run this command in the console:
#    bash <(curl -fsSL https://gitlab.com/neplusultra/provision-scripts/raw/master/cloud9-expo.sh)

# update
echo
echo UPDATING YUM...
sudo yum -y update

# git
echo
echo UPDATING GIT...
sudo yum -y install git
sudo yum -y upgrade git

# install manager (v0.5.1 as of 29-08-18)
echo
echo SETTING UP LANG VERSION MANAGER...
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.5.1

# update bashrc
echo
echo UPDATING BASH_PROFILE...
echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/.bash_profile
echo -e '\n. $HOME/.asdf/completions/asdf.bash' >> ~/.bash_profile

# update session
echo
echo RELOADING BASH_PROFILE...
source ~/.bash_profile

# install dependencies (FEDORA)
echo
echo INSTALLING DEPENDENCIES...
sudo yum -y install automake autoconf readline-devel ncurses-devel openssl-devel libyaml-devel libxslt-devel libffi-devel libtool unixODBC-devel inotify-tools pcre-devel

# install watchman (v4.9.0 as of 29-08-18)
cd $HOME
git clone https://github.com/facebook/watchman.git
cd watchman
git checkout v4.9.0
./autogen.sh
./configure
make
sudo make install
cd ..
sudo rm -Rf ./watchman

# nodejs (10.9.0 as of 29-08-18)
echo
echo SETTING UP NODE.JS...
asdf plugin-add nodejs
rm -rf ~/.asdf/keyrings/nodejs
~/.asdf/plugins/nodejs/bin/import-release-team-keyring
asdf install nodejs 10.9.0
asdf global nodejs 10.9.0

# expo
echo
echo SETTING UP EXPO SDK...
npm install -g exp

echo
echo SETTING UP FIREWALL...
sudo yum -y install ufw
sudo ufw disable
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow www
sudo ufw enable

echo
echo SETTING UP AUTOMATIC UPDATES...
yum -y install yum-cron
sudo sed -i 's/update_cmd = default/update_cmd = security/g' /etc/yum/yum-cron.conf
sudo sed -i 's/apply_updates = no/apply_updates = yes/g' /etc/yum/yum-cron.conf

echo
echo
echo SETUP COMPLETE!
