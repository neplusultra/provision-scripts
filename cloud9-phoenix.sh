#!/usr/bin/env bash

#####################################
########### PHOENIX SETUP ###########
#####################################
############### USAGE ###############
#####################################
#
# 1. Create a new workspace on Cloud9 using the "Blank" template
#
# 2. Run this command in the console:
#    bash <(curl -fsSL https://gitlab.com/neplusultra/provision-scripts/raw/master/cloud9-phoenix.sh)

# update
echo
echo UPDATING YUM...
sudo yum -y update

# git
echo
echo UPDATING GIT...
sudo yum -y install git
sudo yum -y upgrade git

# install manager (v0.5.1 as of 29-08-18)
echo
echo SETTING UP LANG VERSION MANAGER...
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.5.1

# update bashrc
echo
echo UPDATING BASH_PROFILE...
echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/.bash_profile
echo -e '\n. $HOME/.asdf/completions/asdf.bash' >> ~/.bash_profile

# update session
echo
echo RELOADING BASH_PROFILE...
source ~/.bash_profile

# install dependencies (FEDORA)
echo
echo INSTALLING DEPENDENCIES...
sudo yum -y install automake autoconf readline-devel ncurses-devel openssl-devel libyaml-devel libxslt-devel libffi-devel libtool unixODBC-devel

# nodejs (10.9.0 as of 29-08-18)
echo
echo SETTING UP NODE.JS...
asdf plugin-add nodejs
rm -rf ~/.asdf/keyrings/nodejs
~/.asdf/plugins/nodejs/bin/import-release-team-keyring
asdf install nodejs 10.9.0
asdf global nodejs 10.9.0
npm install -g brunch

# erlang (21.0.6 as of 29-08-18)
echo
echo SETTING UP ERLANG...
asdf plugin-add erlang
asdf install erlang 21.0.6
asdf global erlang 21.0.6

# elixir (1.7.3 as of 29-08-18)
echo
echo SETTING UP ELIXIR...
asdf plugin-add elixir
asdf install elixir 1.7.3
asdf global elixir 1.7.3
echo y | mix local.hex
echo y | mix local.rebar

# phoenix
echo
echo SETTING UP PHOENIX...
sudo yum -y install inotify-tools
mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez

# postgresql
echo
echo SETTING UP POSTGRES...
sudo yum -y install postgresql postgresql-server postgresql-devel postgresql-contrib postgresql-docs
sudo service postgresql initdb -e UTF8
sudo service postgresql start
sudo -u postgres createuser -s ec2-user
sudo -u postgres createdb ec2-user
sudo sudo -u postgres psql << EOF
  ALTER USER ec2-user WITH SUPERUSER;
  \q
EOF
sudo sudo -u postgres psql << EOF
  ALTER USER postgres WITH PASSWORD 'postgres';
  UPDATE pg_database SET datistemplate = FALSE WHERE datname = 'template1';
  DROP DATABASE template1;
  CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UTF8';
  UPDATE pg_database SET datistemplate = TRUE WHERE datname = 'template1';
  \c template1
  VACUUM FREEZE;
  \q
EOF
sudo sed -i 's/ident/md5/g' /var/lib/pgsql9/data/pg_hba.conf
sudo service postgresql reload

echo
echo SETTING UP FIREWALL...
sudo yum -y install ufw
sudo ufw disable
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow www
sudo ufw enable

echo
echo SETTING UP AUTOMATIC SECURITY UPDATES...
yum -y install yum-cron
sudo sed -i 's/update_cmd = default/update_cmd = security/g' /etc/yum/yum-cron.conf
sudo sed -i 's/apply_updates = no/apply_updates = yes/g' /etc/yum/yum-cron.conf

echo
echo
echo SETUP COMPLETE!
